################################################################################
# Compute paths
get_filename_component(RapidJSON_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)

set(RapidJSON_INCLUDE_DIR "${RapidJSON_CMAKE_DIR}/../thirdparty/rapidjson/include")
set(RapidJSON_INCLUDE_DIRS "${RapidJSON_CMAKE_DIR}/../thirdparty/rapidjson/include")
message(STATUS "RapidJSON found. Headers: ${RapidJSON_INCLUDE_DIRS}")
