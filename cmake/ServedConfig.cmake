################################################################################
# Compute paths
get_filename_component(Served_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)

set(Served_INCLUDE_DIR "${Served_CMAKE_DIR}/../thirdparty/served/include")
set(Served_INCLUDE_DIRS "${Served_CMAKE_DIR}/../thirdparty/served/include")
set(Served_LIBRARIES "${Served_CMAKE_DIR}/../thirdparty/served/lib/libserved.a")
message(STATUS "Served found. Headers: ${Served_INCLUDE_DIRS}")
message(STATUS "Served found. Library: ${Served_LIBRARIES}")
