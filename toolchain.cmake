# CMake system name must be something like "Linux".
# This is important for cross-compiling.

set( CMAKE_SYSTEM_NAME Linux )
set( CMAKE_SYSTEM_PROCESSOR arm )
set( CMAKE_C_COMPILER arm-poky-linux-gnueabi-gcc )
set( CMAKE_CXX_COMPILER arm-poky-linux-gnueabi-g++ )
set( CMAKE_ASM_COMPILER arm-poky-linux-gnueabi-as )
set( CMAKE_AR arm-poky-linux-gnueabi-ar CACHE FILEPATH "Archiver" )
set( CMAKE_C_FLAGS "  --sysroot=${SDKTARGETSYSROOT}  -O2 -pipe" CACHE STRING "CFLAGS" )
set( CMAKE_CXX_FLAGS "  --sysroot=${SDKTARGETSYSROOT}  -O2 -pipe" CACHE STRING "CXXFLAGS" )
set( CMAKE_ASM_FLAGS "  --sysroot=${SDKTARGETSYSROOT}  -O2 -pipe" CACHE STRING "ASM FLAGS" )
set( CMAKE_C_FLAGS_RELEASE "-DNDEBUG" CACHE STRING "Additional CFLAGS for release" )
set( CMAKE_CXX_FLAGS_RELEASE "-DNDEBUG" CACHE STRING "Additional CXXFLAGS for release" )
set( CMAKE_ASM_FLAGS_RELEASE "-DNDEBUG" CACHE STRING "Additional ASM FLAGS for release" )
set( CMAKE_C_LINK_FLAGS "  --sysroot=${SDKTARGETSYSROOT}  -Wl,-O1" CACHE STRING "LDFLAGS" )
set( CMAKE_CXX_LINK_FLAGS "  --sysroot=${SDKTARGETSYSROOT}  -O2 -pipe -Wl,-O1" CACHE STRING "LDFLAGS" )

# only search in the paths provided so cmake doesnt pick
# up libraries and tools from the native build machine
set( CMAKE_FIND_ROOT_PATH ${SDKTARGETSYSROOT} ${OECORE_NATIVE_SYSROOT} )
set( CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY )
set( CMAKE_FIND_ROOT_PATH_MODE_PROGRAM ONLY )
set( CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY )
set( CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY )

# Use qt.conf settings
set( ENV{QT_CONF_PATH} /home/drybalkin/soft/yocto-rpi/poky-sumo-v2/rpi-build/tmp/work/x86_64-nativesdk-pokysdk-linux/nativesdk-libdnf/0.11.1-r0/qt.conf )

# We need to set the rpath to the correct directory as cmake does not provide any
# directory as rpath by default
set( CMAKE_INSTALL_RPATH  )

# Use native cmake modules
list(APPEND CMAKE_MODULE_PATH "/home/drybalkin/soft/yocto-rpi/poky-sumo-v2/rpi-build/tmp/work/x86_64-nativesdk-pokysdk-linux/nativesdk-libdnf/0.11.1-r0/recipe-sysroot/opt/poky/2.5/sysroots/x86_64-pokysdk-linux/usr/share/cmake/Modules/")

# add for non /usr/lib libdir, e.g. /usr/lib64
set( CMAKE_LIBRARY_PATH /opt/poky/2.5/sysroots/x86_64-pokysdk-linux/usr/lib /opt/poky/2.5/sysroots/x86_64-pokysdk-linux/lib)

