#!/bin/bash

##
# Copyright (c) 2018 Volkswagen AG (EES). All Rights Reserved.
##

# Build script for SOD project.

TOP_DIR="$(dirname $(readlink -f $0))"

export SDK_ROOT=~/soft/yocto-rpi/poky-sumo-v2/rpi-build/tmp"
export VECTOR_DIR=~/vector"
export QEMU_IMAGE_DIR=~/Vector_D05_qemuarm64"

variables_help() {
    echo "Please export Vector SDK, Vector repository, Vector prebuild images folders like the following:"
    echo "  $ export SDK_ROOT=~/vector-sdk"
    echo "  $ export VECTOR_DIR=~/vector"
    echo "  $ export QEMU_IMAGE_DIR=~/Vector_D05_qemuarm64"
    echo
    echo "[OPTIONAL] Also you can provide custom login@ip variables which has default value:"
    echo ' $ export TARGET_LOGIN_IP="root@192.168.7.2"'
    echo
    echo "[OPTIONAL] Also you can provide custom password for target:"
    echo ' $ export TARGET_LOGIN_IP="yourpassword"'
    echo
    echo "[OPTIONAL] Also you can provide custom root directory (for example to use local directory rootfs):"
    echo ' $ export TARGET_ROOT_DIR="/"'
}

if [[ -z "${VECTOR_DIR}" ]]; then
    variables_help
    exit
fi

if [[ -z "${SDK_ROOT}" ]]; then
    variables_help
    exit
fi

if [[ -z "${QEMU_IMAGE_DIR}" ]]; then
    variables_help
    exit
fi

BUILD_DIR=${TOP_DIR}/build
if [[ -z "${TARGET_LOGIN_IP}" ]]; then
    TARGET_LOGIN_IP="root@192.168.7.2"
fi
if [[ -z "${TARGET_ROOT_DIR}" ]]; then
    TARGET_ROOT_DIR="/"
fi

START_QEMU_CMD="source ${SDK_ROOT}/environment-setup-*-poky-linux; \
                runqemu qemu\${ARCH} \
                ${QEMU_IMAGE_DIR}/*Image-qemu\${ARCH}.bin \
                ${QEMU_IMAGE_DIR}/amsr-image-example-dev-qemu\${ARCH}.ext4 \
                nographic"

echo "VECTOR_DIR = ${VECTOR_DIR}"
echo "SDK_ROOT = ${SDK_ROOT}"
echo "QEMU_IMAGE_DIR = ${QEMU_IMAGE_DIR}"
echo "TARGET_LOGIN_IP = ${TARGET_LOGIN_IP}"
echo "TARGET_ROOT_DIR = ${TARGET_ROOT_DIR}"

set_env() {
    source ${SDK_ROOT}/environment-setup-arm1176jzfshf-vfp-poky-linux-gnueabi

    ARCH_BUILD_DIR=${TOP_DIR}/build/${ARCH}
    CMAKE_TOOLCHAIN=${TOP_DIR}/toolchain${ARCH}.cmake

    echo "CMAKE_TOOLCHAIN = ${CMAKE_TOOLCHAIN}"
    echo "ARCH_BUILD_DIR = ${ARCH_BUILD_DIR}"
}

build() {
    set_env

    mkdir -p ${ARCH_BUILD_DIR}

    cd ${ARCH_BUILD_DIR}
    cmake -DCMAKE_TOOLCHAIN_FILE=${CMAKE_TOOLCHAIN} ${TOP_DIR}
    make -j$(nproc --all)
    cd -
}

install() {
    set_env

    rm -rf ${ARCH_BUILD_DIR}/image

    export DESTDIR=${ARCH_BUILD_DIR}/image
    cmake -P ${ARCH_BUILD_DIR}/cmake_install.cmake

    if [[ -z "$1" ]]; then
        echo "Please specify also the software cluster name ('host'|'swcl')"
        exit
    fi

    cmake -DCOMPONENT=$1 -P ${ARCH_BUILD_DIR}/cmake_install.cmake
}

clean() {
    rm -rf ${BUILD_DIR}

    for obj in $(ls ${TOP_DIR}/src-gen | grep -v etc); do
        rm -rf ${TOP_DIR}/src-gen/$obj
    done
}

run_qemu() {
    x-terminal-emulator -e bash -c "${START_QEMU_CMD};bash"
}

deploy() {
    set_env
    scp -rp -v ${ARCH_BUILD_DIR}/image/* ${TARGET_LOGIN_IP}:${TARGET_ROOT_DIR}
}

deploy_viwi() {
    set_env
    scp -rp -v ${ARCH_BUILD_DIR}/viwi_libs_project/* ${TARGET_LOGIN_IP}:${TARGET_ROOT_DIR}
}

undeploy() {
    set_env
    ssh ${TARGET_LOGIN_IP} "rm -rf ${TARGET_ROOT_DIR}/opt/diag*"
}

undeploy_viwi() {
    set_env
    ssh ${TARGET_LOGIN_IP} "rm -rf ${TARGET_ROOT_DIR}/usr/include/ViWi_*"
    ssh ${TARGET_LOGIN_IP} "rm -rf ${TARGET_ROOT_DIR}/usr/lib/libMIB*"
    ssh ${TARGET_LOGIN_IP} "rm -rf ${TARGET_ROOT_DIR}/usr/share/sod_viwi/*"
}

generate() {
    mkdir -p ${BUILD_DIR}

    cd ${TOP_DIR}/model

    SIP="${VECTOR_DIR}/DaVinciConfigurator/Core"
    OUTPUT="$(readlink -f ../src-gen)"

    if [ ! -d "${OUTPUT}" ]; then
       mkdir "${OUTPUT}"
    else
        # There are JSON application configuration files
        # that couldn't be generated
        for dir in `ls -1 ${OUTPUT} | grep -v etc`; do
            rm -rf ${OUTPUT}/${dir}
        done
    fi

    ${VECTOR_DIR}/DaVinciConfigurator/Core/amsrgen.sh \
        -x . \
        -o ${OUTPUT} \
        -g ${VECTOR_DIR}/Generators \
    | tee ${BUILD_DIR}/amsrgen_log.txt \
    | grep -E "\- File.*(\.cc|\.h)" | awk '{print $5}'

    cd -
}

generate_host_dm() {
    mkdir -p ${BUILD_DIR}

    cd ${TOP_DIR}/model

    ./configure_host_dm.sh

    cd -
}

generate_swcl_dm() {
    mkdir -p ${BUILD_DIR}

    cd ${TOP_DIR}/model

    ./configure_swcl_dm.sh

    cd -
}

help() {
    echo "Helper script to build SOD project."
    echo
    echo "Usage:"
    echo "  mk [CMD]"
    echo
    echo "List of supported commands:"
    echo "    build            : compile the project"
    echo "    clean            : remove build folder"
    echo "    deploy           : deploy application to QEMU filesystem"
    echo "    generate         : perform generation with DaVinci"
    echo "    help             : display this information"
    echo "    run_qemu         : start Vector QEMU machine in separate terminal"
    echo "    run_qemu_bg      : start Vector QEMU machine in background"
    echo "    undeploy         : remove content that was deployed to QEMU filesystem by deploy command"
    echo
    echo "If there is no CMD specified, the following sequence will run: generate, build, deploy"
    echo
}

all() {
    clean
    generate
    build
    install
    undeploy
    deploy
}

# Define the default behavior: build everything
if [ "$#" = "0" ]; then
    build
    install
    undeploy
    deploy
else
    COMMAND="$1"; shift
    $COMMAND "$@"
fi
