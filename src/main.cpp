

#include <iostream>
#include <served/served.hpp>
#include <served/plugins.hpp>

int main() {
    // Create a multiplexer for handling requests
    served::multiplexer mux;
    mux.use_after(served::plugin::access_log);

    // Create the server and run with 10 handler threads.
    served::net::server server("0.0.0.0", "11300", mux);
    server.run(10);

    return (EXIT_SUCCESS);
}
